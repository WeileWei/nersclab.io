# Development vs Production Environments

When deploying your application in Spin, you have a choice of using the development and the production environments.
Generally, we would advise that the final, internet-accessible url for your application is served from the production
environment, as it has more memory and storage space than development.

Meanwhile, you can use the development environment for testing a new version of your application with features not yet
enabled in production. Once the new features are tested and you feel they're ready, they can be deployed in production
as well.

If bugs are found in your application, you can test and fix these in the development environment. You can make changes,
delete databases, pause the app for debugging, etc. in development, without fear of impacting actual users.

One way to ensure that you don't accidentally release pre-production features in the production environment is to use
different `git` branches. For example, the development environment could use the "main" or "master" branch, while
production the "prod" branch. You would have to merge "master" to "prod" in order to move your new code into the
production branch.

When building the docker image of a git branch, it can be helpful to tag the image with the branch name, for example
"myapp:prod" vs. "myapp:main". You can automate docker image tags by programmatically getting the current git branch
name:

```bash
BRANCH=`git rev-parse --abbrev-ref HEAD`
TAG=`echo $BRANCH | sed -E 's/[[:space:]]/_/g'`
REPO='registry.nersc.gov/m12345/mycoolapp'
docker build -t "$REPO:$TAG" . && docker push "$REPO:$TAG"
```
