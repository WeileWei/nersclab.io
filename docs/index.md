# NERSC Technical Documentation

[National Energy Research Scientific Computing
(NERSC)](https://www.nersc.gov) provides High Performance Computing (HPC)
and Storage facilities and support for research sponsored by, and of
interest to, the U.S. Department of Energy (DOE) Office of Science
(SC).

## Top documentation pages

* [Getting Started](getting-started.md) - Information for new and existing users
* [Getting Help](getting-started.md#getting-help) - How to get support
* [Job Queue Policy](jobs/policy.md) - Charge factors, run limits, submit limits
* [Example Jobs](jobs/examples/index.md) - Curated example job scripts
* [Jobs overview](jobs/index.md) - [Slurm](https://slurm.schedmd.com/) commands, job script basics, submitting, updating jobs
* [Jupyter](services/jupyter.md) - Interactive [Jupyter Notebooks](https://jupyter.org/) at NERSC
* [Globus](services/globus.md) - High-performance data transfers
* [File permissions](filesystems/unix-file-permissions.md) - Unix file permissions
* [Multi-Factor Authentication (MFA)](connect/mfa.md)

## Computing Resources

* [Perlmutter](systems/perlmutter/index.md) - A Cray EX system with AMD EPYC CPUs and NVIDIA A100 GPUs
* [Cori](systems/cori/index.md) - A Cray XC40 system with Intel Haswell and Intel KNL CPUs

## Other NERSC web pages

* [NERSC Home page](https://nersc.gov) - NERSC news and information
* [MOTD](https://www.nersc.gov/live-status/motd/) - Live status of NERSC services
* [MyNERSC](https://my.nersc.gov) - Interactive content
* [Help Portal](https://help.nersc.gov) - Open support tickets, make resource requests
* [JupyterHub](https://jupyter.nersc.gov) - Access NERSC with interactive notebooks and more
* [Iris](https://iris.nersc.gov) - NERSC account management

!!! tip
    The [NERSC Users Group (NUG)](https://www.nersc.gov/users/NUG/) is an
    independent organization of users of NERSC resources.

    All users are welcome to [join the NUG Slack
    workspace](https://www.nersc.gov/users/NUG/nersc-users-slack/).

!!! info "NERSC welcomes your contributions"
    This project is hosted on [GitLab](https://gitlab.com/NERSC/nersc.gitlab.io) and your
    [contributions](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md)
    are welcome!
